import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Manuais } from '../pages/manuais/manuais';
import { Promocoes } from '../pages/promocoes/promocoes';
import { Orcamento } from '../pages/orcamento/orcamento';
import { Manual } from '../pages/manual/manual';
import { Produtos } from '../pages/produtos/produtos';
import { Produto } from '../pages/produto/produto';
import { TabsPage } from '../pages/tabs/tabs';

import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '7c876f31',
  },
  'push': {
    'sender_id': '655880158498',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#DDDDDD'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    Manuais,
    Promocoes,
    Orcamento,
    Manual,
    Produtos,
    Produto,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Manuais,
    Promocoes,
    Orcamento,
    Manual,
    Produtos,
    Produto,
    TabsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
