import { Component } from '@angular/core';

import { NavController, LoadingController, AlertController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Produto } from '../produto/produto';

@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html'
})
export class Produtos {

  loader: any;
  produtos: any;
  total_produtos: any;
  pesquisa: any = "";

  refresh(refresh){
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/produtos', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.doRefresh(data, refresh),
      err => this.erro_app(err, refresh),
    );
  }

  erro_app(err, refresh){
    refresh.complete();
    this.erro(err);
  }

  doRefresh(data, refresh){
    refresh.complete();
    this.validar(data);
    this.pesquisa = "";
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(err){
    this.carregou();
    let alert = this.alertCtrl.create({
      title: "Erro",
      subTitle: "Verifique sua conexão com a internet",
      buttons: ['OK']
    });
    alert.present();
  }

  validar(data){
    this.total_produtos = data.total_produtos;
    this.produtos = data.produtos;
    this.carregou();
  }

  ver_produto(id){
    this.carregando();
    var post = "id="+id;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/produto', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.push(data),
      err => this.erro(err),
    );
  }

  pesquisar(){
    this.carregando();
    var post = "pesquisa="+this.pesquisa;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/produtos_pesquisa', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar(data),
      err => this.erro(err),
    );
  }

  push(data){
    this.navCtrl.push(Produto, {
      data: data
    });
    this.carregou();
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/produtos', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar(data),
      err => this.erro(err),
    );
  }

}
