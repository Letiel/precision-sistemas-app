import { Component } from '@angular/core';

import { NavController, LoadingController, AlertController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-promocoes',
  templateUrl: 'promocoes.html'
})
export class Promocoes {

  loader: any;
  promocoes: any;

  refresh(refresh){
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/promocoes', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.doRefresh(data, refresh),
      err => this.erro_app(err, refresh),
    );
  }

  erro_app(err, refresh){
    refresh.complete();
    this.erro(err);
  }

  doRefresh(data, refresh){
    refresh.complete();
    this.validar(data);
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  erro(err){
    this.carregou();
    let alert = this.alertCtrl.create({
      title: "Erro",
      subTitle: "Verifique sua conexão com a internet",
      buttons: ['OK']
    });
    alert.present();
  }

  validar(data){
    this.promocoes = data.promocoes;
    this.carregou();
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/promocoes', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar(data),
      err => this.erro(err),
    );
  }

}
