import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-produto',
  templateUrl: 'produto.html'
})
export class Produto {
  produto:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.produto = this.navParams.get('data');
  }

}
