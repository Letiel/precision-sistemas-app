import { Component } from '@angular/core';

import { NavController, LoadingController, AlertController } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Promocoes } from '../promocoes/promocoes';

@Component({
  selector: 'page-orcamento',
  templateUrl: 'orcamento.html'
})
export class Orcamento {
  loader:any;

  nome: any;
  email: any;
  telefone: any;
  empresa: any;
  descricao: any;

  erro(err){
    this.carregou();
    let alert = this.alertCtrl.create({
      title: "Erro",
      subTitle: "Verifique sua conexão com a internet",
      buttons: ['OK']
    });
    alert.present();
  }

  validar(data){
    if(data.sucesso == 's'){
      this.nome = "";
      this.email = "";
      this.telefone = "";
      this.empresa = "";
      this.descricao = "";
      this.navCtrl.setRoot(Promocoes);
    }
    let alert = this.alertCtrl.create({
      title: "Sucesso",
      subTitle: data.msg,
      buttons: ['OK']
    });
    alert.present();
    this.carregou();
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.loader.present();
  }

  carregou() {
    this.loader.dismiss();
  }

  submit = function(){
    this.carregando();
    var post = "nome="+this.nome+"&email="+this.email+"&telefone="+this.telefone+"&empresa="+this.empresa+"&descricao="+this.descricao;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.precisionsistemas.com.br/API/orcamento', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar(data),
      err => this.erro(err),
    );
  }
  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

  }

}
