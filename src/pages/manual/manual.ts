import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-manual',
  templateUrl: 'manual.html'
})
export class Manual {
  manual:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.manual = this.navParams.get('data');
  }

}
