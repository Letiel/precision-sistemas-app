import { Component } from '@angular/core';

import { Orcamento } from '../orcamento/orcamento';
import { Manuais } from '../manuais/manuais';
import { Promocoes } from '../promocoes/promocoes';
import { Produtos } from '../produtos/produtos';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  Promocoes: any = Promocoes;
  Orcamento: any = Orcamento;
  Manuais: any = Manuais;
  Produtos: any = Produtos;

  constructor() {

  }
}
