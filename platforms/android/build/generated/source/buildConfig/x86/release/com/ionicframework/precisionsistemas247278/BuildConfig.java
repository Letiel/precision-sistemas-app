/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ionicframework.precisionsistemas247278;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.ionicframework.precisionsistemas247278";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "x86";
  public static final int VERSION_CODE = 101044;
  public static final String VERSION_NAME = "1.1.4";
}
